// 4-bit RISC Processor

module RISCProcessor(
  input wire clk,
  input wire rst,
  input wire [3:0] instruction,
  input wire [3:0] operand,
  output wire [3:0] result
);

  // Register File
  reg [3:0] reg_file [0:15];

  // ALU Signals
  wire [3:0] alu_out;
  wire zero_flag;

  // ALU
  ALU alu(.a(reg_file[instruction[3:2]]), .b(operand), .op(instruction[1:0]), .out(alu_out), .zero(zero_flag));

  // Register File Write
  always @(posedge clk or posedge rst) begin
    if (rst) begin
      reg_file <= 16'h0;
    end else begin
      if (instruction[3:2] != 4'b0000) begin
        reg_file[instruction[3:2]] <= alu_out;
      end
    end
  end

  // Output Result
  assign result = alu_out;

endmodule

// ALU Module
module ALU(
  input wire [3:0] a,
  input wire [3:0] b,
  input wire [1:0] op,
  output wire [3:0] out,
  output wire zero
);

  // ALU Operations
  parameter ADD = 2'b00;
  parameter SUB = 2'b01;
  parameter AND = 2'b10;
  parameter OR  = 2'b11;

  // ALU Result
  always @* begin
    case (op)
      ADD: out = a + b;
      SUB: out = a - b;
      AND: out = a & b;
      OR:  out = a | b;
      default: out = 4'b0;
    endcase
    zero = (out == 4'b0);
  end

endmodule
