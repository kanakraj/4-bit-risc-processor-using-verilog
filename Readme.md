# 4-Bit RISC Processor

This Verilog project implements a basic 4-bit RISC processor with components as ALU, control unit, register file, memory, and instruction decoder.

## Directory Structure

- `src/`: Contains Verilog source files.
  - `alu.v`: 4-bit ALU implementation.
  - `control_unit.v`: Control unit for instruction sequencing.
  - `register_file.v`: 4-bit register file.
  - `memory.v`: Simple memory module.
  - `instruction_decoder.v`: Decoder for RISC instructions.
  - `processor.v`: Top-level module integrating all components.
  - `testbench.v`: Testbench for simulation.

## Implementation Details

- `alu.v`: The ALU module performs basic arithmetic and logic operations.

- `control_unit.v`: The control unit generates control signals based on the instruction opcode.

- `register_file.v`: The register file stores 4-bit data in registers.

- `memory.v`: The memory module provides storage for instructions and data.

- `instruction_decoder.v`: Decodes the instruction opcode and generates control signals.

- `processor.v`: The top-level module integrating all components to create the 4-bit RISC processor.

- `testbench.v`: A testbench for simulation, providing sample inputs and verifying expected outputs.


3. Run the simulation using the following commands:

   ```bash
   iverilog -o simulator_output processor.v testbench.v
   vvp simulator_output
